DataModel.pptx
- description of tables in GeoBase's sqlite3 database 

GeoBase.xml
- XBee radio configurations for the GeoBase 

GeoBaseDiagrams.pptx
- diagrams explaining GeoBase's operation

GeoBase_NextSteps.docx
- list of TODO and WISH LIST items for GeoBase 

GeoBase_RaspberryPi.docx
- overview of how to operate GeoBase on Pi

GeoBase.xml
- XBee radio configurations for each GeoMote 

GeoMoteDiagrams.pptx
- diagrams explaining GeoMote's operation

GeoMote_ArduinoFio.docx
- overview of how to operate GeoMote

GeoMote_Lab.fzz
- Fritzing breadboard diagram of Arduino Fio + RTC + various sensors
- You'll need the Fritzing app to view: http://fritzing.org/home/

nextSteps.docx
- list of TODO and WISH LIST items for GeoMote

GeoMote_vibratingWirePiezo.fzz
- Fritzing breadboard diagram of Arduino Fio + RTC + Campbell Sci AVW-200

Goals.docx
- short, mid, and long-term goals for this project

LICENSE.txt
- MIT license for all source

SystemOverview.pptx
- general overview of wireless sensor network

XBeeRadio.docx
- how to operate the XBee radios

light_data.jpg
- example data from lab tests

temperature_data.jpg
- example data from lab tests
