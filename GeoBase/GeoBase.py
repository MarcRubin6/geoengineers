# open connection to database, connection to usb (XBEE radio)
#   update mote's time
#   write data and status to sqlite database

import serial, time, dataset, sys

print time.asctime() + " starting GeoBase..."
db_name = 'sqlite:////home/pi/GeoBase/GeoData.sqlite';
xbee_port = '/dev/serial/by-id/usb-FTDI_FT231X_USB_UART_DA01MC0P-if00-port0'
timeout = 10;

#open serial port to XBee radio with 10 second timeout
#   use the "long" path for the radio: (this may need to change)
try:
    ser = serial.Serial(xbee_port, 57600, timeout=timeout);
except Exception as e:
    sys.stderr.write(time.asctime() + " ERROR: could not connect to XBee radio\n");
    sys.exit(1);

#buffer incoming data
data = [];
status = [];

#wait for messages
while True:
    #get the message and remove carriage return and newline
    msg = ser.readline().strip('\r\n');

    #if timeout expired, try to write data to database
    if msg == '':
        if data:
            #if database not available, retry later..
            try:
                db = dataset.connect(db_name)
                db['data'].insert_many(data);
                data = [];
            except Exception as err:
                sys.stderr.write(time.asctime() + " ERROR: " + str(err) + "\n")
                pass;

        if status:
            try:
                db = dataset.connect(db_name)
                db['status'].insert_many(status);
                status = [];
            except Exception as err:
                sys.stderr.write(time.asctime() + " ERROR: " + str(err) + "\n")
                pass;

    #look for TIME query message.. response with UTC time for right now.
    elif "TIME?" in msg:    
        now = int(time.time())
        ser.write(str(now) + "\n")

    #look for DATA message: "D", ID, time, temp, piezo, light
    elif "D," in msg:
        words = msg.split(',');
        
        #put dictionary into data buffer
        if len(words) == 6:
            data.append(dict(   mote=words[1],\
                                time=words[2],\
                                temp=words[3],\
                                piezo=words[4],\
                                light=words[5]      ));

    #look for status message: "S", ID, battery, mote's current time (now())
    elif "S," in msg:
        words = msg.split(',')

        #put dictionary into status buffer
        if len(words) == 4:
            status.append(dict( mote=words[1],\
                                battery=words[2],\
                                mote_time=words[3],\
                                base_time=int(time.time()) ));

