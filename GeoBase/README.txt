GeoBase.py
- Python source to listen for XBee packets, response to TIME? requests, and insert data, status into GeoData.sqlite database

checkGeoBaseRunning.sh
- BASH shell script to check if GeoBase.py is running and restart if necessary

postToServer.sh
- BASH shell script to periodically run post_all.py

post_all.py
- get all data and status updates from GeoData.sqlite, POST to server 

log.txt
- log file for GeoBase.py 

postLog.txt
- log file for post_all.py

web/post_server.py
- example Python Flask server for receive POST requests (This should be replaced with a Geoengineers Cloud service)

