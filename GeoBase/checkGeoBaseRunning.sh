#check to see if there's a running process called GeoBase.py
pgrep -f GeoBase.py > /dev/null 2> /dev/null

#if not, restart GeoBase
if [ $? -ne 0 ]; then
    echo "`date`: ERROR GeoBase is down" >> /home/pi/GeoBase/log.txt
    echo "`date`: restarting..." >> /home/pi/GeoBase/log.txt

    #run command
    /usr/bin/nohup /usr/bin/python -u /home/pi/GeoBase/GeoBase.py >> /home/pi/GeoBase/log.txt &
fi


