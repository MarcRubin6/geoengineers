#check to see if there's a running process called post_all.py
pgrep -f post_all.py > /dev/null 2> /dev/null

#if not, run post_all.py
if [ $? -ne 0 ]; then
    #run command
    /usr/bin/nohup /usr/bin/python -u /home/pi/GeoBase/post_all.py >> /home/pi/GeoBase/postLog.txt 2>> /home/pi/GeoBase/postLog.txt 
fi

