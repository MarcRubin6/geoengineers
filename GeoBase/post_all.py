import sys, time, dataset, requests, json

#URL to post JSON data
url = "http://localhost:5000";

#database to query data
db_name = 'sqlite:////home/pi/GeoBase/GeoData.sqlite'

#table_name to query, route to POST, json_field return value containing primary key (id) to delete from database
def post_to_server( table_name, route, json_field):
    global url, db_name;
    dest = url + route;

    #get most recent data from sqlite db
    while True:
        try:
            db = dataset.connect(db_name);
            result = db[table_name].all();
        except Exception as err:
            continue;
        else:
            break;

    #iterate through data
    for row in result:
		#POST data (in JSON form) to server..
        try:
            resp = requests.post(dest, data=json.dumps(row), headers={'Content-Type': 'application/json'});

        #if request cannot be made, log error and exit (try again later).
        except Exception as err:
            sys.stderr.write(time.asctime() + " ERROR: " + str(err) + "\n")
            sys.exit(1);

        #validate response is 'OK'
        if ( (int(resp.status_code) / 200) == 1):

            #get response object with collection of sqlite IDs that have been saved
            resp_json = json.loads(str(resp.text));
            resp_json = resp_json[0];
            idd = int(resp_json[json_field]);

            #delete ID from database
            while True:
                try:
                    db = dataset.connect(db_name);
                    db[table_name].delete(id=idd);
                    db.commit();
                except Exception as err:
                    continue;
                else:
                    break;

if __name__ == "__main__":
    post_to_server('data', '/data', 'sensor_reading_id');
    post_to_server('status', '/status', 'status_id');
