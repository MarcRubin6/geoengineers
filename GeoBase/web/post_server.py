from flask import Flask
from flask import request
import dataset
import json

db_name = "sqlite:////home/pi/GeoBase/web/GeoData.sqlite"
db = dataset.connect(db_name);
app = Flask(__name__)

#create route to handle incoming data
@app.route('/data', methods = ['GET', 'POST'])
def post_data():
    if request.method == 'POST':
        json_data = request.get_json();

        #parse json dictionary, post to local sqlite DB (omitting primary key id)
        db['data'].insert(dict(light=json_data['light'],\
                                piezo=json_data['piezo'],\
                                mote=json_data['mote'],\
                                temp=json_data['temp'],\
                                time=json_data['time']));

        #send a response (for 200 status);
        retval = '[{"sensor_reading_id":%d}]' % json_data['id'];
        return retval 

#create route to handle incoming status..
@app.route('/status', methods = ['GET', 'POST'])
def post_status():
    if request.method == 'POST':
        json_data = request.get_json();

        #parse json dictionary (omit primary key 'id'), post to local sqlite DB
        db['status'].insert(dict(battery=json_data['battery'], \
                                mote=json_data['mote'], \
                                base_time=json_data['base_time'], \
                                mote_time=json_data['mote_time']));

		#send a response (for 200 status);
        retval = '[{"status_id":%d}]' % json_data['id'];
        return retval

if __name__ == '__main__':
    #app.run(host='0.0.0.0')
    app.run()

