#include <Arduino.h>

#define DEBUG 0

#define MOTE_ID 1
#define TIME 946684800  //Jan 1st,2000 (for error checking)
#define LED 13
#define OFFSET 15 //offset, in seconds, between mote comms
#define LIGHT A2
#define CTS 4
#define XBEE 5

#define SAMPLE_RATE 10 //in seconds
#define COMM_RATE 60 // in seconds
#define SIZE 10


