#include "GeoMote.h"
#include <Time.h>
#include <TimeAlarms.h>
#include <Streaming.h>
#include <Wire.h>
#include <DS3234.h>
#include <TMP_102.h>

DS3234 rtc(8);
TMP_102 tempSensor;

unsigned long times[SIZE];
unsigned int light[SIZE];
float piezo[SIZE];
float temp[SIZE];
float battery;
unsigned int index = 0;

void setup()
{  
  //initialize GeoMote to start synching, sampling, and communicating
  initGeoMote();
}

void loop()
{
  //advance the clock 500 ms
  Alarm.delay(500);
  
  //make sure clock is synched with RTC
  syncWithRTC();
}
