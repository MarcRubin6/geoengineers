*** You MUST install all libraries in the required_libs folder into Arduino's library path. ***
- For my Mac, this is "/Applications/Arduino.app/Contents/Resources/Java/libraries"
- E.g., The DS3234 real-time lock library is located at "/Applications/Arduino.app/Contents/Resources/Java/libraries/DS3234"
- For Windows, see Arduino's documentation for installing libraries

required_libs/
- directory containing all required Arduino libs for this project

GeoMote.h
- header file containing default configuration settings (e.g., SAMPLE_RATE)

GeoMote.ino
- implements setup() and loop() => similar to main()

XBee.ino
- source to control XBee radio sleep and wake

communicate.ino
- implements logic to send status update and data to GeoBase

initGeoMote.ino
- implements code to initialize the GeoMote on boot

sample.ino
- source code to acquire and store sample(s)

syncWithGeoBase.ino
- source code to ask GeoBase and process current time in Unix UTC

syncWithRTC.ino
- synchronizes Arduino Fio's internal clock with external RTC

vibratingWirePiezo.ino
- code to communicate with Campbell Sci AVW-200 and get samples from Slope Indicator Vibrating Wire Piezometer
