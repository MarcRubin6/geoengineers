void beginXBee()
{
  Serial.begin(57600);
  
  //setup pins to sleep radio
  pinMode(XBEE, OUTPUT);
  pinMode(CTS, INPUT);
  
  //put XBee in sleep state
  sleepXBee();
}

void wakeXBee()
{
  //pull DTR pin low
  digitalWrite(XBEE, LOW);
  
  //wait for "Clear To Send" to go HIGH
  while(digitalRead(CTS) == HIGH);
  delayMicroseconds(500);
}

void sleepXBee()
{
  //pull DTR pin HIGH
  digitalWrite(XBEE, HIGH);
}


