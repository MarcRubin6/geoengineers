/*
  send GeoMote's status
 send GeoMote's data
 get settings from GeoBase
 get UTC from GeoBase
 */

void communicate()
{
  //setup next communication in COMM_RATE seconds
  Alarm.timerOnce(COMM_RATE, communicate);

  wakeXBee();

  //send status: mote_ID, battery, now()
  Serial << "S," << MOTE_ID << "," << battery << "," << now() << endl;

  //send data
  for(int i = 0; i < index; i++){
    Serial << "D," << MOTE_ID << "," << times[i] << "," << temp[i] << "," << _FLOAT(piezo[i],6) << "," << light[i] << endl;
  }
  index = 0;

  //synchronize RTC with GeoBase
  syncWithGeoBase();
  
  sleepXBee();
}


