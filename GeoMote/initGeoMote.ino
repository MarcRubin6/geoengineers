void initGeoMote()
{
  pinMode(LIGHT, INPUT);
  
  // called to start collecting data from vibrating wire piezometer
  startVWP(); 
  
  //start radio
  beginXBee();

  //begin connection to RTC
  rtc.begin();
  
//  //FOR TESTING ONLY! -> set time to something arbitrary
//  rtc.setTime(23, 58, 40);       // Set the time to 23:58:40
//  rtc.setDate(31, 12, 2000);    // Set date to 12/31/2000
  
  //synchronize RTC to UTC at GeoBase
  wakeXBee(); 
  syncWithGeoBase();
  sleepXBee();
  
  syncWithRTC();
  
  //start sampling and transmitting at beginning of next minute
  unsigned int h, m;
  getNextMinute(h, m);
  
  //start sampling at beginning of NEXT minute
  Alarm.alarmOnce(h, m, 0, sample);
  Alarm.alarmOnce(h, m, MOTE_ID*OFFSET, communicate);
  
}

void getNextMinute(unsigned int &h, unsigned int &m)
{
  m = minute() + 1;
  h = hour();

  //if next minute rolls into the next hour, shift appropriately
  if(m == 60){
    m = 0;
    h++;
    if(h == 24) h = 0;
  }
}
