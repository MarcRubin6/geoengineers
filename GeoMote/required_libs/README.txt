DS3234/
- source to control real time clock

SDISerial
- implements SDI-12 communication protocol

Streaming
- allows for use of "Serial << " coding (similar to "cout << " in C++) 

TMP_102
- implements firmware for temperature sensor

Time
- get / set Arduino's internal clock

TimeAlarms
- set timers and alarms for state machine

Wire
- used for I2C communicatin protocol (with TMP102 sensor)
