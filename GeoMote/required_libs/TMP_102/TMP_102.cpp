#include "TMP_102.h"

TMP_102::TMP_102()
{
    Wire.begin();
    address = 0x48;
}

float TMP_102::getTemperature()
{
  Wire.requestFrom(address,2); 

  byte MSB = Wire.read();
  byte LSB = Wire.read();

  //it's a 12bit int, using two's compliment for negative
  int TemperatureSum = ((MSB << 8) | LSB) >> 4; 

  float temp = TemperatureSum*0.0625;
  temp = (1.8 * temp) + 32.0;
  return temp;
}
