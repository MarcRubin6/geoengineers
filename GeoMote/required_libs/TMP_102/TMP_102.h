#include <Arduino.h>
#include <Wire.h> //for TMP_102

class TMP_102 {
  private:
    int address;
  public:
    float getTemperature();
    TMP_102();
};


