/*
  obtain samples from sensors
 */

void sample()
{
  //set timer for SAMPLE_RATE seconds from now
  Alarm.timerOnce(SAMPLE_RATE, sample);

  //get current time and data from sensors
  times[index] = now();
  temp[index] = tempSensor.getTemperature();
  piezo[index] = getPSI();
  light[index] = analogRead(A2);
  battery = getBattery();
  
  index++;
}
