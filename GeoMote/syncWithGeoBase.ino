/*
  synchronize RTC w/ GeoBase UTC
 */

void syncWithGeoBase()
{
  unsigned long t = 0;

  //clear input buffer
  Serial.setTimeout(0);
  while(Serial.available()) Serial.read();
  Serial.setTimeout(2000);

  //ask GeoBase for current time (in UTC)
  Serial << MOTE_ID << "," << "TIME?" << endl;

  //wait to receive message in UTC epoch form
  t = Serial.parseInt();

  //make sure time is valid and clock needs updating.. 
  if(t >= TIME && t != now()){
    setTime(t);
  }

  //sync RTC with Mote's clock 
  rtc.setTime(hour(), minute(), second());      
  rtc.setDate(day(), month(), year());   
}



