/*
  synchronize Mote's clock to RTC
 */

void syncWithRTC()
{
  Time t = rtc.getTime();

  //sync clock with RTC
  setTime(t.hour, t.min, t.sec, t.date, t.mon, t.year);
}


