/*code for fault-tolerant vibrating wire piezometer*/
#include <math.h>
#include <stdio.h>
#include <SDI12.h>
#include <Time.h>

//calibration factors for SLope Meter Vibrating Wire Piezometer
#define PSI_A -1.778564e-5
#define PSI_B 2.030652e-3
#define PSI_C 1.368960e2

#define KPA_A -1.226277e-4
#define KPA_B 1.400085e-2
#define KPA_C 9.438646e2

#define THM_A 1.40e-3
#define THM_B 2.37e-4
#define THM_C 9.90e-8

//correction due to elevation in Bend, OR
#define PSI_ELV 1.914870
#define KPA_ELV 13.203171

//time to wait for response from AVW-200
#define DELAY 1000

//time to wait before timing out
#define TIMEOUT 5

//SDI-12 address of AVW-200
int addr;

//6 data points received from AVW-200 after measurement request (see AVW-200 manual)
float freq, rms, snr, noise, decay, therm, _batt;
char msg[64], *ptr;
int _index = 0;

//pin to connect to Campbell AVW-200
#define AVW_PIN 9        
SDI12 mySDI12(AVW_PIN); 

//THIS MUST BE CALLED ONCE!!
void startVWP()
{
  mySDI12.begin(); 
  delay(2000);
}

//get piezo measurement in Pounds per Square Inch
float getPSI()
{
  //check for timeout, return obvious number
  if(_getSample() == false) return -999;

  //calculate psi and return
  return PSI_A * (freq * freq) + (PSI_B * freq) + PSI_C + PSI_ELV; 
}

//get piezo measurement in kPa (kiloPascals)
float getKPA()
{
  //check for timeout, return obvious number
  if(_getSample() == false) return -999;

  //calculate kPa and return
  return (KPA_A * (freq * freq)) + (KPA_B * freq) + KPA_C + KPA_ELV;
}

//get temperature from piezo's built-in thermistor 
float getThermistorTemp()
{
  //check for timeout, return obvious value
  if(_getSample() == false) return -999;
  return (1.0 / (THM_A + (THM_B * log(therm)) + (THM_C * pow(log(therm),3)))) - 273.15;
}

//get raw data from AVW-200.  Geotechs may want this
void getRawData(float rawData[6])
{
  //if timeout occurs, set rawData to obvious numbers and return
  if(_getSample() == false) {
    for(int i = 0; i < 6; i++) rawData[i] = -999;  
    return; 
  }
  
  rawData[0] = freq;
  rawData[1] = rms;
  rawData[2] = snr;
  rawData[3] = noise;
  rawData[4] = decay;
  rawData[5] = therm;
}

//returns voltage of 12V battery attached to AVW-200
float getBattery()
{
  //watch for timeouts
  if(_requestBattery() == false) return -999;  
  if(_waitForResponse() == false) return -999;

  //send command to retrieve frist line of data: address "+" battery "+" trapcodes "+" watchdog counts
  //wait 1 second for data to arrive 
  mySDI12.sendCommand("0D0!");  
  delay(DELAY);

  _getMsg();

  //tokenize by "+" and ignore FIRST token (i.e., address field)
  ptr = strtok(msg,"+");
  addr = atoi(ptr);
  _batt = _getNextFloat();

  return _batt;
}

bool _getSample()
{
  //check for timeouts
  if(_requestMeasurement() == false) return false;  
  if(_waitForResponse() == false) return false;
  
  _getFreqRmsSnr();
  _getNoiseDecayTherm();
  
  return true;
}

//helper function to grab message from AVW-200
void _getMsg()
{
  _index = 0;
  memset(msg, 0x00, 64);
  while(mySDI12.available()) {
    msg[_index++] = mySDI12.read();
  } 
}

//hardcoded to AVW-200 channel 1
bool _requestMeasurement()
{
  //ask for Measurement from address 0, channel 1
  mySDI12.sendCommand("0M1!");
  
  //check for timeouts
  if(_getAck()) return true;
  else return false;
}

bool _requestBattery()
{
  //send command to read battery levels from address 0
  mySDI12.sendCommand("0V!");
  
  //check for timeouts
  if(_getAck()) return true;
  else return false;
}

//returns true on success, false on timeout
bool _waitForResponse()
{
  //get mote's current time
  unsigned long t1 = now();
  
  //wait for a response "a\n\r" from AVW-200, where a = address of device (e.g., 0), \n is newline, \r is carriage return
  while(mySDI12.available() < 3){
    
    //check to see if TIMEOUT seconds have elapsed. 
    if( (now() - t1 > TIMEOUT)) return false; 
  }
  
  //read in response "a\n\r"
  for(int i = 0; i < 3; i++){
    mySDI12.read();
  }
  
  //everything OK
  return true;
}

void _getFreqRmsSnr()
{
  //send command to retrieve frist line of data (delimited by "+") 
  //wait 1 second for data to arrive 
  mySDI12.sendCommand("0D0!");  
  delay(DELAY);

  //read data:
  _getMsg();

  //order is: address+freq+rms+snr
  ptr = strtok(msg,"+");
  
  //check to see if ptr valid.  If so, convert ASCII to integer.  if not, set to -999
  addr = (ptr != NULL) ? atoi(ptr) : -999;
  freq = _getNextFloat();
  rms = _getNextFloat();
  snr = _getNextFloat();
}

void _getNoiseDecayTherm()
{
  //send command to retrieve 2nd line of data: 
  mySDI12.sendCommand("0D1!");  
  delay(DELAY);

  _getMsg();

  //order is: address+noise+decay+therm
  ptr = strtok(msg,"+");
  
  //check to see if ptr valid.  If so, convert ASCII to integer.  if not, set to -999
  addr = (ptr != NULL) ? atoi(ptr) : -999; 
  noise = _getNextFloat();
  decay = _getNextFloat();
  therm = _getNextFloat();
}

float _getNextFloat()
{
  ptr = strtok(NULL, "+");
  
  //check to see if ptr is valid.  if not, return -999
  if(ptr == NULL) return -999;
  return atof(ptr);
}

//returns true on success, false on timeout
bool _getAck()
{
  unsigned long t1 = now();
  
  //wait for 7 characters in response: "atttn\n\r", where a = address of device, ttt = seconds to wait, n = samples to expect 
  while(mySDI12.available() < 7){
    
    //check to see if TIMEOUT seconds have elapsed. 
    if( (now() - t1) > TIMEOUT) return false;
  }
  
  //read in "atttn\n\r"
  for(int i = 0; i < 7; i++){
    mySDI12.read();
  }
  
  //everything's OK
  return true;
}


