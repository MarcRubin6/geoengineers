The source code and documentation herein implements an Arduino-based wireless sensor network prototype system.  Currently, the system can collect data from a: vibrating wire piezometer, light sensor, and temperature sensor.  Be sure to peruse the Documentation before delving in to the source.

Documentation/
- contains all documentation related to the wireless sensor network prototype

GeoMote/
- contains all source code and required Arduino libraries for GeoMote

GeoBase/
- contains copies of all source code and scripts residing on GeoBase (Raspberry Pi)

Datasheets/
- contains datasheets and manuals related to this project: e.g., Campbell Scientific AVW-200, XBee radios, etc.

code_exps/
- contains small tidbits of simple test code

data.csv
- example raw data from lab tests

status.csv
- example status updates from lab tests
