#include <Time.h>
#include <TimeAlarms.h>
#include <Streaming.h>
#include <Wire.h>
#include <TMP_102.h>

#define MOTE_ID 3
#define TIME 946684800  //Jan 1st,2000 (for error checking)
#define LED 13
#define OFFSET 10 //offset, in seconds, between mote comms
#define PIEZO A0
#define LIGHT A2
#define SAMPLE_RATE 60 //in seconds

unsigned long t;
unsigned int piezo;
unsigned int light;
float temp, battery = 100;

TMP_102 tempSensor;

void setup()
{  
  //LED for debugging
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);

  pinMode(PIEZO, INPUT);
  pinMode(LIGHT, INPUT);

  //setup radio and set timeout
  Serial.begin(57600);

  //synchronize clock 
  synchronize();

  //start sampling and transmitting at beginning of next minute
  int m = minute() + 1;
  int h = hour();
  if(m == 60){  
    m = 0;
    h++;
    if(h == 24) h = 0;    
  }
  Alarm.alarmOnce(h, m, 0, startSampleTransmit);    
}

void loop()
{
  //advance the clock 
  Alarm.delay(0);
}

void startSampleTransmit()
{
  //set timer for sampleRate seconds
  Alarm.timerRepeat(SAMPLE_RATE, sample);
}

void synchronize()
{
  Serial << MOTE_ID << "," << "TIME?" << endl;

  //wait for receive message in UTC epoch form
  Serial.flush();
  unsigned long t = Serial.parseInt();

  //make sure time is valid.. 
  if(t >= TIME){
    setTime(t);
  }
}

void sample()
{
  //get current time
  t = now();
  temp = tempSensor.getTemperature();
  piezo = analogRead(A0);
  light = analogRead(A2);

  Alarm.timerOnce(MOTE_ID * OFFSET, comm);
}

void comm()
{
  //send status
  Serial << "S," << MOTE_ID << "," << battery << endl;
  
  //send data
  Serial << "D," << MOTE_ID << "," << t << "," << temp << "," << piezo << "," << light << endl;
  
  synchronize();
}

