#include <DS3234.h>
#include <Time.h>
#include <TimeAlarms.h>
#include <Streaming.h>

#define TIME 946684800  //Jan 1st,2000 (for error checking)

// Init the DS3234
DS3234 rtc(8);

void setup()
{
  // Initialize the rtc object
  rtc.begin();

  // Setup Serial connection
  Serial.begin(57600);
  while (!Serial);

  //ask GeoBase for current time (in UTC)
  Serial << "1,TIME?" << endl;

  //wait to receive message in UTC epoch form
  Serial.flush();
  unsigned long t = Serial.parseInt();

  //make sure time is valid.. 
  if(t >= TIME){
    setTime(t);
  }

  //sync RTC with Mote's clock 
  rtc.setTime(hour(), minute(), second());      
  rtc.setDate(day(), month(), year());   

  //  // The following lines can be uncommented and edited to set the time and date in the DS3234
  //  //rtc.setDOW(SATURDAY);        // Set Day-of-Week to SATURDAY
  //  rtc.setTime(6, 34, 27);       // Set the time to 6:34:27 (24hr format)
  //  rtc.setDate(30, 7, 2015);    // Set the date to7/30/2015

  setSyncProvider(sync);
  setSyncInterval(10);

  unsigned int h, m;
  getNextMinute(h, m);

  //sample once per minute 
  AlarmId sampleAlarm = Alarm.timerRepeat(60, sample);    
//  time_t sampleAlarmTime = Alarm.read(sampleAlarm);
  Serial << "sample alarm set for: " << Alarm.read(sampleAlarm) << endl;
  
  //communicate once per minute + some offset
  AlarmId commAlarm = Alarm.alarmOnce(h, m, 15, communicate);
  Serial << "comm alarm set for: " << Alarm.read(commAlarm) << endl;
  
}

//void    setTime(int hr,int min,int sec,int day, int month, int yr);
void loop()
{
  Alarm.delay(0);
  
  Serial << "DEBUG " << now() << endl;
  delay (5000);
}

void sample()
{
  Serial << "DEBUG " << now() << ": sampling... " << endl;

  unsigned int h, m;
  getNextMinute(h, m);

  //once per minute
  //  Alarm.alarmOnce(h, m, 0, sample);   
}

void communicate()
{
  Serial << "DEBUG " << now() << ": communicate... " << endl;

  unsigned int h, m;
  getNextMinute(h, m);

  //once per minute
  Serial << "DEBUG " << now() << " setting alarm for " << h << " " << m << " 15" << endl;
  Alarm.alarmOnce(h, m, 15, communicate);   
}

time_t sync()
{  
  Time t = rtc.getTime();
  setTime(t.hour, t.min, t.sec, t.date, t.mon, t.year);

  Serial << "DEBUG " << now() << " setting clock to: " << t.hour << ":" << t.min << ":" << t.sec 
          << " " << t.mon << "/" << t.date << "/" << t.year << endl;
  return now();
}

void getNextMinute(unsigned int &h, unsigned int &m)
{
  m = minute() + 1;
  h = hour();

  //if next minute rolls into the next hour, shift appropriately
  if(m == 60){
    m = 0;
    h++;
    if(h == 24) h = 0;
  }
}



