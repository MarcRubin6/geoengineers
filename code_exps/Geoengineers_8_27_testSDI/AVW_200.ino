#include <math.h>
#include <stdio.h>

//calibration factors
#define PSI_A -1.778564e-5
#define PSI_B 2.030652e-3
#define PSI_C 1.368960e2

#define KPA_A -1.226277e-4
#define KPA_B 1.400085e-2
#define KPA_C 9.438646e2

#define THM_A 1.40e-3
#define THM_B 2.37e-4
#define THM_C 9.90e-8

//correction due to elevation in Bend
#define PSI_ELV 1.914870
#define KPA_ELV 13.203171

//time to wait for data
#define DELAY 1000

int addr;
float freq, rms, snr, noise, decay, therm;

float getPSI()
{
  _requestMeasurement();
  _waitForResponse();

  _getFreqRmsSnr();
  _getNoiseDecayTherm();

  //calculate psi and return
  float psi = PSI_A * (freq * freq) + (PSI_B * freq) + PSI_C + PSI_ELV; 
  return psi;
}

float getKPA()
{
  _requestMeasurement();
  _waitForResponse();

  _getFreqRmsSnr();
  _getNoiseDecayTherm();

  //calculate psi and return
  float kpa = (KPA_A * (freq * freq)) + (KPA_B * freq) + KPA_C + KPA_ELV;
  return kpa;
}

void getRawData(float rawData[6])
{

}

//returns voltage of 12V battery attached to AVW-200
float getBattery()
{
  char msg[64], *ptr;
  int index = 0;
  float battery = 0;

  _requestBattery();
  _waitForResponse();

  //send command to retrieve frist line of data: address "+" battery "+" trapcodes "+" watchdog counts
  //wait 1 second for data to arrive 
  mySDI12.sendCommand("0D0!");  
  delay(DELAY);

  while(mySDI12.available()) {
    msg[index++] = mySDI12.read();
  }
  
  //tokenize by "+" and ignore FIRST token (i.e., address field)
  ptr = strtok(msg,"+");
  if(ptr != NULL) addr = atoi(ptr);

  ptr = strtok (NULL, "+");
  if(ptr != NULL) battery = atof(ptr);

  return battery;
}

void _requestBattery()
{
  //send command to read battery levels from address 0
  mySDI12.sendCommand("0V!");

  //wait for 7 characters in response: "atttn\n\r", where a = address of device, ttt = seconds to wait, n = samples to expect 
  while(mySDI12.available() < 7);
  for(int i = 0; i < 7; i++){
    mySDI12.read();
  }
}

//hardcoded to AVW-200 channel 1
void _requestMeasurement()
{
  Serial << "0M1!" << endl;
  mySDI12.sendCommand("0M1!");

  //wait for 7 characters in response: "atttn\n\r", where a = address of device, ttt = seconds to wait, n = samples to expect 
  while(mySDI12.available() < 7);
  for(int i = 0; i < 7; i++){
    mySDI12.read();
  }
}

void _waitForResponse()
{
  //wait for a response "a\n\r" from AVW-200, where a = address of device
  while(mySDI12.available() < 3);
  for(int i = 0; i < 3; i++){
    mySDI12.read();
  }
}

void _getFreqRmsSnr()
{
  char msg[64];
  char *ptr;
  int index = 0;

  //send command to retrieve frist line of data (delimited by "+") 
  //wait 1 second for data to arrive 
  Serial << "0D0!" << endl;
  mySDI12.sendCommand("0D0!");  
  delay(DELAY);

  //reset contents of message 
  memset(msg, 0x00, 64);

  //read data:
  while(mySDI12.available()) {
    msg[index++] = mySDI12.read();
  }

  //parse message for "+" (ignore address token)
  ptr = strtok(msg,"+");

  //order is: address+freq+rms+snr
  addr = atoi(ptr);

  ptr = strtok (NULL, "+");
  if(ptr != NULL) freq = atof(ptr);

  ptr = strtok (NULL, "+");
  if(ptr != NULL) rms = atof(ptr);

  ptr = strtok (NULL, "+");
  if(ptr != NULL) snr = atof(ptr);

}

void _getNoiseDecayTherm()
{
  char msg[64];
  int index = 0;
  char *ptr;

  //send command to retrieve 2nd line of data: 
  Serial << "0D1!" << endl;
  mySDI12.sendCommand("0D1!");  
  delay(DELAY);

  while(mySDI12.available()) {
    msg[index++] = mySDI12.read();
  }

  ptr = strtok(msg,"+");

  //order is: address+noise+decay+therm
  addr = atoi(ptr);

  ptr = strtok (NULL, "+");
  if(ptr != NULL) noise = atof(ptr);

  ptr = strtok (NULL, "+");
  if(ptr != NULL) decay = atof(ptr);

  ptr = strtok (NULL, "+");
  if(ptr != NULL) therm = atof(ptr);
}




