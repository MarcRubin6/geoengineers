#include <SDI12.h>
#include <Streaming.h>

#define AVW_PIN 9        
SDI12 mySDI12(AVW_PIN); 

//float piezoData[9]; // { 0: freq, 1: rms, 2: snr, 3: noise, 4: decay, 5: therm, 6: kPa (est.), 7: psi (est.), 8: temp C (est.) }

void setup(){
  mySDI12.begin(); 
  delay(2000);

  Serial.begin(57600); 
  Serial.flush();
}

void loop(){
  
  //use AVW-200 to sample the VW Piezo for 6 data points
  float psi = getPSI();
  Serial << _FLOAT(psi,6) << endl;

  //use AVW-200 to sample the VW Piezo for 6 data points
  float kpa = getKPA();
  Serial << _FLOAT(kpa,6) << endl;
  
  float battery = getBattery();
  Serial << _FLOAT(battery,2) << endl;
    
}
