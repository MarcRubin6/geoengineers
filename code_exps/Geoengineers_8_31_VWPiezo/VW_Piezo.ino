#include <math.h>
#include <stdio.h>
#include <SDI12.h>

//calibration factors
#define PSI_A -1.778564e-5
#define PSI_B 2.030652e-3
#define PSI_C 1.368960e2

#define KPA_A -1.226277e-4
#define KPA_B 1.400085e-2
#define KPA_C 9.438646e2

#define THM_A 1.40e-3
#define THM_B 2.37e-4
#define THM_C 9.90e-8

//correction due to elevation in Bend
#define PSI_ELV 1.914870
#define KPA_ELV 13.203171

//time to wait for data
#define DELAY 1000

int addr;
float freq, rms, snr, noise, decay, therm, battery;

char msg[64], *ptr;
int _index = 0;

//pin to connect to Campbell AVW-200
#define AVW_PIN 9        
SDI12 mySDI12(AVW_PIN); 

void startVWP()
{
  mySDI12.begin(); 
  delay(2000);
}

float getPSI()
{
  _getSample();

  //calculate psi and return
  return PSI_A * (freq * freq) + (PSI_B * freq) + PSI_C + PSI_ELV; 
}

float getKPA()
{
  _getSample();

  //calculate kPa and return
  return (KPA_A * (freq * freq)) + (KPA_B * freq) + KPA_C + KPA_ELV;
}

float getThermistorTemp()
{
  _getSample();
  return (1.0 / (THM_A + (THM_B * log(therm)) + (THM_C * pow(log(therm),3)))) - 273.15;
}

void getRawData(float rawData[6])
{
  _getSample();
  
  rawData[0] = freq;
  rawData[1] = rms;
  rawData[2] = snr;
  rawData[3] = noise;
  rawData[4] = decay;
  rawData[5] = therm;
}

//returns voltage of 12V battery attached to AVW-200
float getBattery()
{
  _requestBattery();
  _waitForResponse();

  //send command to retrieve frist line of data: address "+" battery "+" trapcodes "+" watchdog counts
  //wait 1 second for data to arrive 
  mySDI12.sendCommand("0D0!");  
  delay(DELAY);

  _getMsg();

  //tokenize by "+" and ignore FIRST token (i.e., address field)
  ptr = strtok(msg,"+");
  addr = atoi(ptr);
  battery = _getNextFloat();

  return battery;
}

void _getSample()
{
  _requestMeasurement();
  _waitForResponse();

  _getFreqRmsSnr();
  _getNoiseDecayTherm();
}

void _getMsg()
{
  _index = 0;
  memset(msg, 0x00, 64);
  while(mySDI12.available()) {
    msg[_index++] = mySDI12.read();
  } 
}

//hardcoded to AVW-200 channel 1
void _requestMeasurement()
{
  mySDI12.sendCommand("0M1!");

  //wait for 7 characters in response: "atttn\n\r", where a = address of device, ttt = seconds to wait, n = samples to expect 
  while(mySDI12.available() < 7);
  for(int i = 0; i < 7; i++){
    mySDI12.read();
  }
}

void _requestBattery()
{
  //send command to read battery levels from address 0
  mySDI12.sendCommand("0V!");

  //wait for 7 characters in response: "atttn\n\r", where a = address of device, ttt = seconds to wait, n = samples to expect 
  while(mySDI12.available() < 7);
  for(int i = 0; i < 7; i++){
    mySDI12.read();
  }
}

void _waitForResponse()
{
  //wait for a response "a\n\r" from AVW-200, where a = address of device
  while(mySDI12.available() < 3);
  for(int i = 0; i < 3; i++){
    mySDI12.read();
  }
}

void _getFreqRmsSnr()
{
  //send command to retrieve frist line of data (delimited by "+") 
  //wait 1 second for data to arrive 
  mySDI12.sendCommand("0D0!");  
  delay(DELAY);

  //read data:
  _getMsg();

  //order is: address+freq+rms+snr
  ptr = strtok(msg,"+");
  addr = atoi(ptr);
  freq = _getNextFloat();
  rms = _getNextFloat();
  snr = _getNextFloat();
}

void _getNoiseDecayTherm()
{
  //send command to retrieve 2nd line of data: 
  mySDI12.sendCommand("0D1!");  
  delay(DELAY);

  _getMsg();

  //order is: address+noise+decay+therm
  ptr = strtok(msg,"+");
  addr = atoi(ptr);
  noise = _getNextFloat();
  decay = _getNextFloat();
  therm = _getNextFloat();
}

float _getNextFloat()
{
  ptr = strtok(NULL, "+");
  return atof(ptr);
}


