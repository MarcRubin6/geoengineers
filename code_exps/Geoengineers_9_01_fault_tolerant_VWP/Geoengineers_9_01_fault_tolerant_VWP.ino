#include <Streaming.h>

void setup(){
  startVWP(); // called to start collecting data from vibrating wire piezometer

  Serial.begin(57600);   
  Serial.flush();
}

void loop(){
  
  //use AVW-200 to sample the VW Piezo for 6 data points
  float psi = getPSI();
  Serial << "psi = " << _FLOAT(psi,6) << endl;

//  //use AVW-200 to sample the VW Piezo for 6 data points
//  float kpa = getKPA();
//  Serial << "kPa = " << _FLOAT(kpa,6) << endl;
//  
//  float temp = getThermistorTemp();
//  Serial << "thermistor = " << _FLOAT(temp, 6) << " C" << endl;
//  
   float battery = getBattery();
   Serial << "battery = " << _FLOAT(battery,2) << endl;
//
  float all[6];
  getRawData(all);
  
  Serial << "\t" << "freq = "  << _FLOAT(all[0],6) << endl;
  Serial << "\t" << "rms = "   << _FLOAT(all[1],6) << endl;
  Serial << "\t" << "snr = "   << _FLOAT(all[2],6) << endl;
  Serial << "\t" << "noise = " << _FLOAT(all[3],6) << endl;
  Serial << "\t" << "decay = " << _FLOAT(all[4],6) << endl;
  Serial << "\t" << "therm = " << _FLOAT(all[5],6) << endl;

}

