Geoengineers_7_30_Mote/
- implementation of GeoMote 

Geoengineers_7_30_testRTC
- integrate Fio w/ RTC chip

Geoengineers_8_27_testSDI
- integrate Arduino with AVW-200

Geoengineers_8_31_VWPiezo
- integrate Arduino with AVW-200 + vibrating wire piezo

Geoengineers_9_01_fault_tolerant_VWP
- integrate Arduino w/ AVW-200 + vibrating wire piezo; implement fault tolerance (timeouts)
